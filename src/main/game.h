#ifndef JEU_H_INCLUDED
#define JEU_H_INCLUDED

#include <SDL/SDL.h>

void jouer(SDL_Surface *ecran, char *path);
void deplacerJoueurFenetre(int carte[][NOMBRES_BLOCS_HAUTEUR_CARTE], SDL_Rect *position, int direction, int *positionBlocsFenetreX, \
int *positionBlocsFenetreY);
void deplacerMonstre(int carte[][NOMBRES_BLOCS_HAUTEUR_CARTE], SDL_Rect position[], int sens[]);
int intersectionSprawlMonstre(SDL_Rect *positionJ, SDL_Rect positionM[], SDL_Rect *positionInit, int *positionBlocFenetreX, \
int *positionBlocFenetreY, int nombresDeVies);

#endif

