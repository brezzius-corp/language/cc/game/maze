#ifndef CONSTANTES_H_INCLUDED
#define CONSTANTES_H_INCLUDED

    #define PERIODE                        10 /* PERIODE pour la fonction SDL_PollEvent */

    #define TAILLE_BLOC                    34
    #define HAUTEUR_SPRAWL                 30
    #define LARGEUR_SPRAWL                 22
    #define LARGEUR_MONSTRE                32
    #define HAUTEUR_MONSTRE                32
    #define NB_MONSTRE			   4
    #define NOMBRES_BLOCS_LARGEUR          13 /* Nombre de blocs de la fenêtre */
    #define NOMBRES_BLOCS_HAUTEUR          13
    #define NOMBRES_BLOCS_LARGEUR_CARTE    40 /* Taille de blocs de la carte */
    #define NOMBRES_BLOCS_HAUTEUR_CARTE    40
    #define LARGEUR_FENETRE                TAILLE_BLOC * NOMBRES_BLOCS_LARGEUR
    #define HAUTEUR_FENETRE                TAILLE_BLOC * NOMBRES_BLOCS_HAUTEUR

    enum {HAUT, BAS, GAUCHE, DROITE};
    enum {VIDE, MUR, SPRAWL, MONSTRE, OBJECTIF}; /* On initialise VIDE à 0, MUR à 1, SPRAWL à 2, MONSTRE à 3, OBJECTIF à 4 */

#endif

