#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>

#include "constants.h"
#include "level.h"

int chargerNiveaux(int niveau[][NOMBRES_BLOCS_HAUTEUR_CARTE], char *path)
{
    FILE* fichier = NULL;
    /* On lit les caractères du fichier un à un. On raajoute +1 à la fin pour le '\0' */
    char ligneFichier[NOMBRES_BLOCS_LARGEUR_CARTE * NOMBRES_BLOCS_HAUTEUR_CARTE + 1] = {0}, rscPath[255];
    int i = 0, j = 0;

    sprintf(rscPath, "%s/../src/main/resources/level.txt", path);
    fichier = fopen(rscPath, "r");

    if(fichier == NULL)
    {
        fprintf(stderr, "Impossible d'ouvrir le fichier level.txt: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    fgets(ligneFichier, NOMBRES_BLOCS_LARGEUR_CARTE * NOMBRES_BLOCS_HAUTEUR_CARTE + 1, fichier);

    for(i = 0 ; i < NOMBRES_BLOCS_LARGEUR_CARTE ; i++)
    {
        for(j = 0 ; j < NOMBRES_BLOCS_HAUTEUR_CARTE ; j++)
        {
            switch(ligneFichier[(i * NOMBRES_BLOCS_LARGEUR_CARTE) + j])
            {
                case '0':
                    niveau[j][i] = 0; /* Si le cractère vaut 0, on lui attribut la valeur 0, cf. fichier constantes.h */
                    break;
                case '1':
                    niveau[j][i] = 1; /* Si le cractère vaut 1, on lui attribut la valeur 1 */
                    break;
                case '2':
                    niveau[j][i] = 2; /* Si le cractère vaut 2, on lui attribut la valeur 2 */
                    break;
                case '3':
                    niveau[j][i] = 3; /* Si le cractère vaut 3, on lui attribut la valeur 3 */
                    break;
                case '4':
                    niveau[j][i] = 4; /* Si le cractère vaut 4, on lui attribut la valeur 4 */
                    break;
            }
        }
    }

    fclose(fichier);
    return 1;
}

