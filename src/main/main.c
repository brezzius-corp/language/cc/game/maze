#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>

#include "constants.h"
#include "game.h"

int main(int argc, char *argv[])
{
    SDL_Surface *ecran = NULL, *menu = NULL;
    SDL_Rect positionMenu = {0, 0, 0, 0};
    SDL_Event event;

    int continuer = 1, i;

    char rscPath[255];

    if(argc < 1) {
        fprintf(stderr, "Bad format\n");
        exit(EXIT_FAILURE);
    }

    if(SDL_Init(SDL_INIT_VIDEO) == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    ecran = SDL_SetVideoMode(0, 0, 0, SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_FULLSCREEN);
    if(ecran == NULL)
    {
        fprintf(stderr, "Impossible de charger le mode vidéo: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_WM_SetCaption("Labyrinthe", NULL);

    for(i = strlen(argv[0]) - 1 ; i >= 0 ; i--) {
        if(argv[0][i] != '/')
            argv[0][i] = '\0';
        else
            break;
    }

    sprintf(rscPath, "%s/../src/main/resources/menu.bmp", argv[0]);
    printf("%s\n", rscPath);
    menu = SDL_LoadBMP(rscPath);

    while(continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
                break;
            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_q:
                        continuer = 0;
                        break;
                    case SDLK_1:
                        jouer(ecran, argv[0]);
                        continuer = 0;
                        break;
                    default:
                        continuer = 1;
                        break;
                }
                break;
        }

        SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran -> format, 255, 255, 255));
        SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
        SDL_Flip(ecran);
    }

    SDL_FreeSurface(menu);
    SDL_Quit();

    return EXIT_SUCCESS;
}
