#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <SDL/SDL.h>

#include "constants.h"
#include "game.h"
#include "level.h"

void jouer(SDL_Surface *ecran, char *path)
{
    SDL_Surface *sprawl[4] = {NULL} , *sprawlActuel = NULL;
    SDL_Surface *mur = NULL, *monstre = NULL, *objectif = NULL, *partieGagnee = NULL, *partiePerdu = NULL;
    SDL_Surface *nombresVies3 = NULL, *nombresVies2 = NULL, *nombresVies1 = NULL;
    SDL_Rect position = {0, 0, 0, 0}, positionJoueur = {0, 0, 0, 0}, positionMonstre[NB_MONSTRE] = { {0, 0, 0, 0} }, positionObjectif = {0, 0, 0, 0}, positionInitiale = {0, 0, 0, 0};
    SDL_Rect positionPartieGagnee = {0, 0, 0, 0}, positionPartiePerdu = {0, 0, 0, 0}, positionNombresVies = {0, 442, 0, 0};
    SDL_Event event;

    int continuer = 1, i = 0, j = 0, k = 0;
    int carte[NOMBRES_BLOCS_LARGEUR_CARTE][NOMBRES_BLOCS_HAUTEUR_CARTE] = {0};
    int positionBlocsFenetreX = ((NOMBRES_BLOCS_LARGEUR_CARTE / 2) - (LARGEUR_SPRAWL / 2));
    int positionBlocsFenetreY = ((NOMBRES_BLOCS_HAUTEUR_CARTE / 2) - (HAUTEUR_SPRAWL / 2));
    int positionBlocsJoueurX = 0, positionBlocsJoueurY = 0, positionBlocsMonstreX = 0, positionBlocsMonstreY = 0;
    int tempsActuel = 0, tempsPrecedent = 0; /* Pour la fonction SDL_PollEvent */
    int sens[NB_MONSTRE] = {0}; /* montee = 0, descente = 1 */
    int nombresVies = 3; /* Vie initaiale du joueur */

    char rscPath[255];

    sprintf(rscPath, "%s/../src/main/resources/wall.bmp", path);
    mur = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/goal.bmp", path);
    objectif = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/monster.bmp", path);
    monstre = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/win.bmp", path);
    partieGagnee = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/lose.bmp", path);
    partiePerdu = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/life_3.bmp", path);
    nombresVies3 = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/life_2.bmp", path);
    nombresVies2 = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/life_1.bmp", path);
    nombresVies1 = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/sprawl_down.bmp", path);
    sprawl[BAS] = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/sprawl_high.bmp", path);
    sprawl[HAUT] = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/sprawl_right.bmp", path);
    sprawl[DROITE] = SDL_LoadBMP(rscPath);

    sprintf(rscPath, "%s/../src/main/resources/sprawl_left.bmp", path);
    sprawl[GAUCHE] = SDL_LoadBMP(rscPath);

    for(i = 0 ; i < 4 ; i++)
    {
        SDL_SetColorKey(sprawl[i], SDL_SRCCOLORKEY, SDL_MapRGB(sprawl[i] -> format, 255, 255, 255));
    }

    sprawlActuel = sprawl[BAS];

    /* On charge le niveau */
    if(!chargerNiveaux(carte, path))
    {
        exit(EXIT_FAILURE);
    }
    /* On attribut à SPRAWL, MONSTRE ET OBJECTIF les différentes positions */
    for(i = 0 ; i < NOMBRES_BLOCS_LARGEUR_CARTE ; i++)
    {
        for(j = 0 ; j < NOMBRES_BLOCS_HAUTEUR_CARTE ; j++)
        {
            if(carte[i][j] == SPRAWL)
            {
                positionJoueur.x = i * TAILLE_BLOC;
                positionJoueur.y = j * TAILLE_BLOC;
                positionInitiale.x = positionJoueur.x;
                positionInitiale.y = positionJoueur.y;
                carte[i][j] = VIDE;
            }

           if(carte[i][j] == MONSTRE)
           {
                positionMonstre[k].x = i * TAILLE_BLOC;
                positionMonstre[k].y = j * TAILLE_BLOC;

                carte[i][j] = VIDE;
	        k++;
           } 

            if(carte[i][j] == OBJECTIF)
            {
                positionObjectif.x = i * TAILLE_BLOC;
                positionObjectif.y = j * TAILLE_BLOC;
                carte[i][j] = VIDE;
            }
        }
    }

    SDL_EnableKeyRepeat(10, 10);

    while(continuer)
    {
        /* Fonction pour faire avancer le monstre automatiquement */
        tempsActuel = SDL_GetTicks();
        if(tempsActuel - tempsPrecedent > PERIODE)
        {
            tempsPrecedent = tempsActuel;
            SDL_PollEvent(&event);

            switch(event.type)
            {
                case SDL_QUIT:
                    continuer = 0;
                    break;

                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_q:
                            continuer = 0;
                            break;
                        case SDLK_UP:
                            sprawlActuel = sprawl[HAUT];
                            deplacerJoueurFenetre(carte, &positionJoueur, HAUT, &positionBlocsFenetreX, &positionBlocsFenetreY);
                            break;
                        case SDLK_DOWN:
                            sprawlActuel = sprawl[BAS];
                            deplacerJoueurFenetre(carte, &positionJoueur, BAS, &positionBlocsFenetreX, &positionBlocsFenetreY);
                            break;
                        case SDLK_RIGHT:
                            sprawlActuel = sprawl[DROITE];
                            deplacerJoueurFenetre(carte, &positionJoueur, DROITE, &positionBlocsFenetreX, &positionBlocsFenetreX);
                            break;
                        case SDLK_LEFT:
                            sprawlActuel = sprawl[GAUCHE];
                            deplacerJoueurFenetre(carte, &positionJoueur, GAUCHE, &positionBlocsFenetreX, &positionBlocsFenetreX);
                            break;
                        default:
                            continuer = 1;
                            break;
                    }
                    break;
            }

            /* On appel la fonction deplacerMonstre */
            deplacerMonstre(carte, positionMonstre, sens);

            SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran -> format, 255, 255, 255));

            /* On affiche les murs et objectif à partir de la carte à l'écran */
            for(i = positionBlocsFenetreX ; i < positionBlocsFenetreX + NOMBRES_BLOCS_LARGEUR ; i++)
            {
                for(j = positionBlocsFenetreY ; j < positionBlocsFenetreY + NOMBRES_BLOCS_HAUTEUR ; j++)
                {
                    position.x = (i - positionBlocsFenetreX) * TAILLE_BLOC;
                    position.y = (j - positionBlocsFenetreY) * TAILLE_BLOC;

                    switch(carte[i][j])
                    {
                        case MUR:
                            SDL_BlitSurface(mur, NULL, ecran, &position);
                            break;
                        case OBJECTIF:
                            SDL_BlitSurface(objectif, NULL, ecran, &position);
                            break;
                    }
                }
            }

            /* On affiche le joueur à l'écran */
            positionBlocsJoueurX = positionJoueur.x / TAILLE_BLOC;
            positionBlocsJoueurY = positionJoueur.y / TAILLE_BLOC;

            if((positionBlocsJoueurX >= positionBlocsFenetreX) && \
            (positionBlocsJoueurX <= positionBlocsFenetreX + NOMBRES_BLOCS_LARGEUR) && \
            (positionBlocsJoueurY >= positionBlocsFenetreY) && \
            (positionBlocsJoueurY <= positionBlocsFenetreY + NOMBRES_BLOCS_HAUTEUR))
            {
                position.x = positionJoueur.x - (positionBlocsFenetreX * TAILLE_BLOC);
                position.y = positionJoueur.y - (positionBlocsFenetreY * TAILLE_BLOC);
                SDL_BlitSurface(sprawlActuel, NULL, ecran, &position);
            }

            /* On affiche le monstre à l'écran */
	        for(k = 0 ; k < NB_MONSTRE ; k++) {
            	positionBlocsMonstreX = positionMonstre[k].x / TAILLE_BLOC;
            	positionBlocsMonstreY = positionMonstre[k].y / TAILLE_BLOC;

            	if((positionBlocsMonstreX >= positionBlocsFenetreX) && \
            	(positionBlocsMonstreX <= positionBlocsFenetreX + NOMBRES_BLOCS_LARGEUR) && \
            	(positionBlocsMonstreY >= positionBlocsFenetreY) && \
            	(positionBlocsMonstreY <= positionBlocsFenetreY + NOMBRES_BLOCS_HAUTEUR))
            	{
                    position.x = positionMonstre[k].x - (positionBlocsFenetreX * TAILLE_BLOC);
                    position.y = positionMonstre[k].y - (positionBlocsFenetreY * TAILLE_BLOC);
                    SDL_BlitSurface(monstre, NULL, ecran, &position);
         	    }
	        }

            /* Appel de la fonction intersectionSprawlMonstre */
            nombresVies = intersectionSprawlMonstre(&positionJoueur, positionMonstre, &positionInitiale, \
            &positionBlocsFenetreX, &positionBlocsFenetreY, nombresVies);

            if(nombresVies == 3)
            {
                SDL_BlitSurface(nombresVies3, NULL, ecran , &positionNombresVies);
            }

            else if(nombresVies == 2)
            {
                SDL_BlitSurface(nombresVies2, NULL, ecran, &positionNombresVies);
            }

            else if(nombresVies == 1)
            {
                SDL_BlitSurface(nombresVies1, NULL, ecran, &positionNombresVies);
            }

            else if(nombresVies == 0)
            {
                SDL_BlitSurface(partiePerdu, NULL, ecran, &positionPartiePerdu);
            }

            if(positionJoueur.x == positionObjectif.x && positionJoueur.y == positionObjectif.y)
            {
                SDL_BlitSurface(partieGagnee, NULL, ecran, &positionPartieGagnee);
                SDL_Flip(ecran);
                sleep(5);
                continuer = 0;
            }

            SDL_Flip(ecran);
        }

        else
        {
            SDL_Delay(PERIODE - (tempsActuel - tempsPrecedent)); /* Fin de la fonction SDL_PollEvent */
        }
    }

    /* On libère la mémoire */
    SDL_EnableKeyRepeat(0, 0);

    SDL_FreeSurface(mur);
    SDL_FreeSurface(objectif);
    SDL_FreeSurface(monstre);

    for(i = 0 ; i < 4 ; i++)
    {
        SDL_FreeSurface(sprawl[i]);
    }

}

void deplacerJoueurFenetre(int carte[][NOMBRES_BLOCS_HAUTEUR_CARTE], SDL_Rect *position, int direction, int *positionBlocsFenetreX, \
int *positionBlocsFenetreY)
{
    int posblocx = ((position -> x) / TAILLE_BLOC);
    int posblocy = ((position -> y) / TAILLE_BLOC);

    switch(direction)
    {
        case HAUT:
            /* Si il rencontre le haut de la carte, on arrête */
            if(position -> y - 1 < 0)
                break;
            /* Si il rencintre un mur, on arrête */
            if(carte[(position -> x) / TAILLE_BLOC][(position -> y - 1) / TAILLE_BLOC] == MUR || \
            carte[(position -> x + LARGEUR_SPRAWL) / TAILLE_BLOC][(position -> y - 1) / TAILLE_BLOC] == MUR)
                break;
            position -> y--; /* On fait avancer sprawl de 1 pixel */

            /* Si le personnage est dans la fenêtre... */
            if(((position -> y) / TAILLE_BLOC) - 2 == *positionBlocsFenetreY && \
            *positionBlocsFenetreY > 0)
            {
                (*positionBlocsFenetreY)--; /* On bouge l'écran */
            }
            break;

        case BAS:
            if(posblocy + 1 >= NOMBRES_BLOCS_HAUTEUR_CARTE)
                break;
            if(carte[(position -> x) / TAILLE_BLOC][((position -> y) / TAILLE_BLOC + 1)] == MUR || \
            carte[(position -> x + LARGEUR_SPRAWL) / TAILLE_BLOC][((position -> y) / TAILLE_BLOC + 1)] == MUR)
                break;
            position -> y++;


            if((((position -> y) + HAUTEUR_SPRAWL) / TAILLE_BLOC) + 2 == *positionBlocsFenetreY + NOMBRES_BLOCS_HAUTEUR && \
            *positionBlocsFenetreY + NOMBRES_BLOCS_HAUTEUR < NOMBRES_BLOCS_HAUTEUR_CARTE)
            {
                (*positionBlocsFenetreY)++;
            }
            break;

        case DROITE:
            if(posblocx + 1 >= NOMBRES_BLOCS_LARGEUR_CARTE)
                break;
            if(carte[((position -> x) / TAILLE_BLOC) + 1][(position -> y) / TAILLE_BLOC] == MUR || \
            carte[((position -> x) / TAILLE_BLOC) + 1][(position -> y + HAUTEUR_SPRAWL)  / TAILLE_BLOC] == MUR)
                break;
            position -> x++;
            if((((position -> x) + LARGEUR_SPRAWL) / TAILLE_BLOC) + 2 == *positionBlocsFenetreX + NOMBRES_BLOCS_LARGEUR && \
            *positionBlocsFenetreX  + NOMBRES_BLOCS_LARGEUR < NOMBRES_BLOCS_LARGEUR_CARTE)
            {
                (*positionBlocsFenetreX)++;
            }
            break;

        case GAUCHE:
            if(position -> x - 1 < 0)
                break;
            if(carte[(position -> x - 1) / TAILLE_BLOC][(position -> y) / TAILLE_BLOC] == MUR || \
            carte[(position -> x - 1) / TAILLE_BLOC][(position -> y + HAUTEUR_SPRAWL) / TAILLE_BLOC] == MUR)
                break;
            position -> x--;

            if(((position -> x) / TAILLE_BLOC) - 2 == *positionBlocsFenetreX && \
            *positionBlocsFenetreX > 0)
            {
                (*positionBlocsFenetreX)--;
            }
            break;
    }
}

void deplacerMonstre(int carte[][NOMBRES_BLOCS_HAUTEUR_CARTE], SDL_Rect position[], int sens[])
{
    int k = 0;

    for(k = 0 ; k < NB_MONSTRE ; k++)
    {
    	int posblocx = (position[k].x) / TAILLE_BLOC;
    	int posblocy = (position[k].y) / TAILLE_BLOC;

    	/* Si le monstre rencontre le haut de la carte ou un mur */
    	if(posblocy - 1 < 0 || carte[posblocx][posblocy] == MUR)
    	{
            sens[k] = 1; /* On le fait changer de direction */
        }

    	else if(posblocy + 1 >= NOMBRES_BLOCS_HAUTEUR_CARTE || carte[posblocx][posblocy + 1] == MUR)
    	{
            sens[k] = 0; /* On le fait changer de direction */
    	}
    
    	if(sens[k] == 0)
        {
    	     (position[k].y)--;
    	}

    	else
    	{
    	    (position[k].y)++;
    	}
    }
}

int intersectionSprawlMonstre(SDL_Rect *positionJ, SDL_Rect positionM[], SDL_Rect *positionInit, int *positionBlocFenetreX, \
int *positionBlocFenetreY, int nombresDeVies)
{
    int intersection = 1;
    int k = 0;

        /* Si le monstre et sprawl se croisent... */
	for(k = 0 ; k < NB_MONSTRE && intersection == 1 ; k++)
	{
            if((positionJ -> x > positionM[k].x && positionJ -> x < positionM[k].x + LARGEUR_MONSTRE) && \
            (positionJ -> y > positionM[k].y && positionJ -> y < positionM[k].y + HAUTEUR_MONSTRE))
            {
            	intersection = 0; /* On met la variable intersection à 0 */
            }

            else if((positionJ -> x + LARGEUR_SPRAWL > positionM[k].x && positionJ -> x + LARGEUR_SPRAWL < positionM[k].x + LARGEUR_MONSTRE) && \
            (positionJ -> y > positionM[k].y && positionJ -> y < positionM[k].y + HAUTEUR_MONSTRE))
            {
            	intersection = 0;
            }

            else if((positionJ -> x > positionM[k].x && positionJ -> x < positionM[k].x + LARGEUR_MONSTRE) && \
            (positionJ -> y + HAUTEUR_SPRAWL > positionM[k].y && positionJ -> y + HAUTEUR_SPRAWL < positionM[k].y + HAUTEUR_MONSTRE))
            {
            	intersection = 0;
            }

            else if((positionJ -> x + LARGEUR_SPRAWL > positionM[k].x && positionJ -> x + LARGEUR_SPRAWL < positionM[k].x + LARGEUR_MONSTRE) && \
            (positionJ -> y + HAUTEUR_SPRAWL > positionM[k].y && positionJ -> y + HAUTEUR_SPRAWL < positionM[k].y + HAUTEUR_MONSTRE))
            {
            	intersection = 0;
            }
	}

         /* Si la variable intersection vaut 0 */
	 if(intersection == 0)
        {
            nombresDeVies--; /* On diminue le nombre de vie du joueur */
            positionJ -> x = positionInit -> x; /* On le remet à sa position initiale */
            positionJ -> y = positionInit -> y;
            *positionBlocFenetreX = ((NOMBRES_BLOCS_LARGEUR_CARTE / 2) - (LARGEUR_SPRAWL / 2));
            *positionBlocFenetreY = ((NOMBRES_BLOCS_HAUTEUR_CARTE / 2) - (HAUTEUR_SPRAWL / 2));
        }

        return nombresDeVies;
}

