# Makefile

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
LDFLAGS=-lSDL
EXECUTABLE=maze
EXECUTABLE_TEST=maze_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: maze
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/game.o $(SRC_BIN)/level.o $(LDFLAGS)

test: package maze_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

maze: init main.o game.o level.o
	$(CC) -o $(SRC_BIN)/maze $(SRC_BIN)/main.o $(SRC_BIN)/game.o $(SRC_BIN)/level.o $(CFLAGS) $(LDFLAGS)

maze_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/constants.h $(SRC_MAIN)/game.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

game.o: $(SRC_MAIN)/game.c $(SRC_MAIN)/game.h $(SRC_MAIN)/constants.h $(SRC_MAIN)/level.h
	$(CC) -o $(SRC_BIN)/game.o -c $(SRC_MAIN)/game.c $(CFLAGS)

level.o: $(SRC_MAIN)/level.c $(SRC_MAIN)/level.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/level.o -c $(SRC_MAIN)/level.c $(CFLAGS)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

